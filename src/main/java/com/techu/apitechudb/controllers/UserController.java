package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
//@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(
            @RequestParam(name = "$orderBy", required = false) String orderBy
    ){
        System.out.println("getUsers");
  //      Sort sort = new SpringDataWebProperties.Sort(Sort.Direction.ASC.("age"));
        return new ResponseEntity<>(
                this.userService.findAll(orderBy),
                HttpStatus.OK
        );
    }
    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id){
        System.out.println("Get user by id");
        System.out.println("La id del usuario a buscar es " + id);

        Optional<UserModel> result = this.userService.findById(id);

        return new ResponseEntity<>(
                //new ProductModel(),
                result.isPresent() ? result.get() : "Usuario no encontrado",
                result.isPresent() ?  HttpStatus.OK :  HttpStatus.NOT_FOUND

        );
        // (CONDICION) ? VALE_ESTO_SI TRUE : VALE_ESTO_SI FALSE
    }
    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user){
        System.out.println("addUser");
        System.out.println("La id del usuario a crear es " + user.getId());
        System.out.println("El nombre del usuario a crear es " + user.getName());
        System.out.println("La edad del usuario a crear es " + user.getAge());

        return new ResponseEntity<>(
                this.userService.add(user),
                HttpStatus.CREATED
        );
    }
    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(
            @RequestBody UserModel user, @PathVariable String id
    ){
        System.out.println("updateUser");
        System.out.println("La id del usuario a actualizar en parametro URL es " + id);
        System.out.println("La id del usuario a actualizar es " + user.getId());
        System.out.println("El nombre del usuario a actualizar  es " + user.getName());
        System.out.println("La edad del usuario a actualizar  es " + user.getAge());

        Optional<UserModel> userToUpdate = this.userService.findById(id);

        if (userToUpdate.isPresent()){
            System.out.println("Usuario para actualizar encontrado, actualizando");

            this.userService.update(user);
        }


        return new ResponseEntity<>(
                user,
                userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUsers(@PathVariable String id){
        System.out.println("deleteUser");
        System.out.println("La id del usuario a borrar es " + id);

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "Usuario borrado" : "Usuario no encontrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
//                new ProductModel(),
//                HttpStatus.OK
        );
    }
}

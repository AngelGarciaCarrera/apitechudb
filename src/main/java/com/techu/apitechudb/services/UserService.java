package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;
    public List<UserModel> findAll(String orderBy) {
        System.out.println("findAll en UserService");

        List<UserModel> result;

        if (orderBy != null) {
            result = this.userRepository.findAll(Sort.by("age").descending());
        } else {
            result = this.userRepository.findAll();
        }
        return result;
    }
    public UserModel add(UserModel user){
        System.out.println("add en UserService");

        return this.userRepository.save(user);
    }
    public UserModel update(UserModel user) {
        System.out.println("update en UserService");

        return this.userRepository.save(user);
    }
    public boolean delete(String id) {
        System.out.println("delete en userService");

        boolean result = false;

        if (this.findById(id).isPresent() == true) {
            System.out.println("Usuario encontrado, borrando");

            this.userRepository.deleteById(id);
            result = true;
        }

        return result;
    }
    public Optional<UserModel> findById (String id){
        System.out.println("findBy Id en productService");

//        if (this.objectProperty.getProperty().getAnotherProperty().getYetAnotherProperty())

        return this.userRepository.findById(id);

    }
}





/**

 * BasicCalculator sut = new BasicCalculator();
 * sut.suma (2, 2) -> 4 -> Invariante
 * sut.suma(-16, -5);
 * sut.suma(21111111111111111111111111111111111111111116, 5);
 *
 * if (sut.suma(2, 2) == 4){
 * GREEN -> TEST PASA
 * } else {
 *     RED -> TEST NO PASA
 * }
 *
 * Aserciones
 *
 * AssertEquals(sut.suma(2,2), 4)
 *              (Actual, Expected)
 *
 * CurrencyConverter sut = new CurrencyConverter();
 *
 * sut.convert("EUR", "USD", 3);
 *
 * convert a nivel interno
 *
 * CurrencyRateFetcher crf = new CurrencyRateFetcherMock();
 * crf.fetch("EUR");
 * CurrencyRate usdRate = crf.fetch("USD"); -> esto es un mock
 * //Yo al mock le digo el ratio de conversion que quiero.
 *
 * BasicCalculator bc = Mockgenerator.get("BasicCalculator");
 * return = bc.mult(3, usdRate);
 *
 * Mocks -> Stub, Mock, Spy
 * -> Stub devuelve un valor o justifica si se ha llamado.
 *
**/

